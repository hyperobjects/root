#!/bin/bash
function waitingPressAnyKeyForExit {
  printf "\nThe script has been terminated with an error!\n"
  printf "\n\n"
  read -n 1 -s -r -p "Please press any key to exit..."
}

function checkErrorCode {
  if [ $? -ne 0 ]; then
    waitingPressAnyKeyForExit
    exit 1
  fi
}

set -o pipefail
./mvnw clean install -P doc | tee building.log
checkErrorCode
cd ui-react || exit
checkErrorCode
./mvnw clean install -P cypress | tee ../cypress.log
checkErrorCode
cd - || exit
waitingPressAnyKeyForExit
exit 1
