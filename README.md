# HyperObjects Project Root

## Prerequisite
1. JDK 17
2. Stand-alone Git
3. Docker

## Clone all subprojects at once
1. Create the `hyperobjects` root folder in your working space. You can choose another name if you want.
2. Execute the following git command inside that folder
```shell
git clone --recurse-submodules https://gitlab.com/hyperobjects/root.git ./
```
It clones the entire project in the previously tested surely stable state.

## Switching projects snapshots
The project is organized as a git of git-submodules. Each of git-submodules is a separate Maven
module. The `master` branch of the root project always contains a `Tested` snapshot of all submodules,
and can guarantee consistency and compatibility among all git-submodules. Meanwhile, each of
git-submodules can be independently developed further and can have `Latest` updates in its
`dev` branch. Until the `Latest` snapshot becomes `Tested` and committed to the root `master`, the
git-submodules can be temporary incompatible among each other. To manage these 2 snapshots in a
convenient way, we have the `switch-snapshot.sh` script in the root project. This script has 2
options, that can be provided as user's answers in the interactive mode or as script's command line
parameters

| Option                                                                 | CLI parameter | Valid values           | Description                                                                                                                                                                                                                                                                                                                       |
|------------------------------------------------------------------------|---------------|------------------------|-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Clean working directories, indexes and reset `master` & `dev` branches | --clean       | Y - yes, N - No (skip) | To avoid problems during the pulling and updating working directories and incompatibilities during building, the script can hard reset all submodules automatically. **BE REALLY CAREFUL!!!**  Use this option, only if you are absolutely sure, you have committed all important changes in all submodules into feature branches |
| Selected snapshot                                                      | --snapshot    | T - tested, L - latest | A selected snapshot to switch the entire project into                                                                                                                                                                                                                                                                             |

**Windows users:** If you installed standalone `Git` on your machine, you can run the script just double-click it

## Run DB as Docker container
The project obligatory uses the MySQL/MariaDB in run-time and for all integration tests.
1. Authenticate your Docker in GitLab:
```shell script
docker login registry.gitlab.com
Username: <your-gitlab-login>
Password: <your-gitlab-password>
Login Succeeded
```
2. Run in the .\app-be-test\ directory.
```shell
docker-compose up -d
```

## Install and run MariaDB locally (if you cannot or don't want to run Docker)
https://mariadb.org/download/

## Build and test the whole project
**Precondition:** In a localized OS Windows 10, you need to create the system environment variable running the comand with administrator rights:
```shell script
setx /M JAVA_TOOL_OPTIONS "-Dfile.encoding=UTF-8"
```
1. Common building
```shell
./mvnw clean install
```
2. Assembling source-files into artifacts
```shell
./mvnw clean install -Psrc
```
3. Assembling javadocs into artifacts
```shell
./mvnw clean install -Pdoc
```
4. Building with Cypress UI e2e tests
```shell
./mvnw clean install -Pcypress
```
5. Running application after building
```shell
cd app-be-test
./mvnw spring-boot:run
```

## Configure your IDE
### Google-Java-Format
The project uses the google-java-format maven plugin, and it automatically reformats your code-base 
during each maven building. To use this feature during development you should additionally
preconfigure your IDE. There is an official instruction how to adjust
[IntelliJ, Android Studio, and other JetBrains IDEs](https://github.com/google/google-java-format/blob/master/README.md#intellij-android-studio-and-other-jetbrains-ides)