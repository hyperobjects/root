#!/bin/bash
function waitingPressAnyKeyForExit {
  printf "\nThe script has been terminated with an error!\n"
  printf "\n\n"
  read -n 1 -s -r -p "Please press any key to exit..."
}

function checkErrorCode {
  if [ $? -ne 0 ]; then
    waitingPressAnyKeyForExit
    exit 1
  fi
}

function cleanWorkingDir {
  echo "Cleaning up the working directory..."
  git submodule foreach --recursive git clean -fdx && git clean -fdx
  checkErrorCode
  git submodule foreach --recursive git reset --hard && git reset --hard
  checkErrorCode
}

function updateMasterRoot {
  echo "Update root master..."
  git checkout master
  checkErrorCode
  git pull origin master --no-rebase --ff-only
  checkErrorCode
}

function switchToLatestVersion {
  echo "Switching to the latest version..."
  git submodule foreach --recursive git checkout dev
  checkErrorCode
  git submodule foreach --recursive git pull origin dev
  checkErrorCode
}

function switchToTestedVersion {
  switchToLatestVersion
  echo "Switching to the tested version..."
  git submodule update --init --recursive
  checkErrorCode
}

function displayHelp {
  printf "\nUsage:  sh update.sh [OPTIONS]\n"
  printf "\nOptions:\n"
  printf "\n--clean      For cleaning up the working directory, indexes, and branches (Y|y) or to skip it (N|n)"
  printf "\n--snapshot   For switching to the latest (L|l) or tested (T|t) snapshot"
  printf "\n-h, --help   To display the help"
  printf "\n\n"
  exit 1
}

while [[ $# -gt 0 ]]; do
  case "$1" in
  -h | --help)
    displayHelp
    ;;
  --clean)
    if [[ $2 =~ ^[YyNn]$ ]]; then
      cleanAnswer="$2"
      shift
    else
      displayHelp
    fi
    ;;
  --snapshot)
    if [[ $2 =~ ^[TtLl]$ ]]; then
      switchAnswer="$2"
      shift
    else
      displayHelp
    fi
    ;;
  *)
    displayHelp
    ;;
  esac
  shift
done

while [ -z "$cleanAnswer" ]; do
  printf "\nATTENTION! Selecting 'yes' will remove all uncommitted changes and development environment settings from the working directories\n"
  printf "\n"
  echo "Would you like to clean up the working directory (Y/N)?"
  read cleanAnswer
  if ! [[ $cleanAnswer =~ ^[YyNn]$ ]]; then
    unset cleanAnswer
  fi
done
while [ -z "$switchAnswer" ]; do
  echo "Switch to (T) the tested or (L) the latest version of the submodules?"
  read switchAnswer
  if ! [[ $switchAnswer =~ ^[TtLl]$ ]]; then
    unset switchAnswer
  fi
done

updateMasterRoot

printf "\n"
case "$cleanAnswer" in
y | Y)
  cleanWorkingDir
  ;;
n | N)
  echo "Cleaning has been skipped"
  ;;
*)
  displayHelp
  ;;
esac

printf "\n"
case "$switchAnswer" in
t | T)
  switchToTestedVersion
  ;;
l | L)
  switchToLatestVersion
  ;;
*)
  displayHelp
  ;;
esac
exit 0
