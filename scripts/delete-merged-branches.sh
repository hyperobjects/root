#!/usr/bin/env bash

OPTIONS="--effort 3 --remote origin --exclude master --yes"

if [[ ! -d venv ]]
then
  sudo apt-get update
  sudo apt-get install git python3-virtualenv python3 -y
  git config --global --add credential.helper cache
  virtualenv venv
  source venv/bin/activate
  pip3 install --upgrade pip
  pip3 install git-delete-merged-branches
else
  source venv/bin/activate
fi

git submodule foreach "git-delete-merged-branches ${OPTIONS}"
git-delete-merged-branches "${OPTIONS}"
deactivate
