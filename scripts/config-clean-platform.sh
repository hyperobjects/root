#!/bin/bash
function waitingPressAnyKeyForExit {
  printf "\n\n"
  read -n 1 -s -r -p "Please press any key to exit..."
}

function checkErrorCode {
  if ! [ $? -eq 0 ]; then
    printf "\nThe script has been terminated with an error!\n"
    waitingPressAnyKeyForExit
    exit $?
  fi
}

PATH_TO_YAML_FILE="../*/src/main/resources/liquibase/*.yaml"
PATH_TO_YML_FILE="../*/src/main/resources/liquibase/*.yml"
PATH_TO_APPLICATION_FILE="../app-be-test/src/main/resources/application.yml"
PATTERN_NAME_DESIRED_FILE_FOR_SED="/\(.*\)-test-data\(.*\)/ s/^/#/"
PATTERN_INCLUDE_STRING="/include:/ {N; /\(.*\)-test-data\(.*\)/ s/^/#/"

printf "\nThe Script is running...\n"
# Exclude the removed scripts from the databaseChangeLog includes
if [[ "$OSTYPE" == "darwin"* ]]; then
find .. -path "$PATH_TO_YAML_FILE" -o -path "$PATH_TO_YML_FILE" -exec sed -e "$PATTERN_NAME_DESIRED_FILE_FOR_SED" -i '' {} \; -exec sed -e "$PATTERN_INCLUDE_STRING;}" -i '' {} +
checkErrorCode
else
  find .. -path "$PATH_TO_YAML_FILE" -o -path "$PATH_TO_YML_FILE" -exec sed -e "$PATTERN_NAME_DESIRED_FILE_FOR_SED" -i {} \; -exec sed -e "$PATTERN_INCLUDE_STRING}" -i {} +
  checkErrorCode
fi
# Provide params for database with empty data
printf "
server.port: 8081
hyperobjects.persistence.connection.jdbc.database: hyperobjects_pump
hyperobjects.persistence.connection.jdbc.username: root
hyperobjects.persistence.connection.jdbc.password: 123456" >> "$PATH_TO_APPLICATION_FILE"

printf "\nThe script has been completed!\n"
waitingPressAnyKeyForExit
exit 0
