@echo off
CALL mvnw clean install -P doc
if errorlevel 1 goto :error
cd ui-react
CALL mvnw clean install -P cypress
if errorlevel 1 goto :error
pause
exit /b

:error
pause
exit /b %errorlevel%
